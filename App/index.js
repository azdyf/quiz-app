import { createStackNavigator, createAppContainer } from 'react-navigation';
import QuizIndex from './screen/QuizIndex'
import Quiz from './screen/Quiz';
import { registerRootComponent } from 'expo';

const MainStack = createStackNavigator({
  QuizIndex: {
    screen: QuizIndex,
    navigationOptions: {
      headerTitle: "Quizzes"
    }
  },
  Quiz: {
    screen: Quiz,
    navigationOptions: ({ navigation }) => ({
      headerTitle: navigation.getParam('title'),
      headerTintColor: '#fff',
      headerStyle: {
        backgroundColor: navigation.getParam('color'),
        borderBottomColor: navigation.getParam('color'),
      }
    }),
  }
})

export default registerRootComponent(createAppContainer(MainStack));

