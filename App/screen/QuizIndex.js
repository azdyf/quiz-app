import React from 'react';
import { ScrollView, Button, StatusBar } from 'react-native';

import spaceQuestion from '../data/space'
import westernsQuestion from '../data/westerns'
import computerQuestion from '../data/computers'

import { RowItem } from '../components/RowItem';

export default ({ navigation }) => (
  <ScrollView>
    <StatusBar barStyle='dark-content' />
    <RowItem 
      color='#36b1f0' 
      name='Space' 
      onPress={() => 
        navigation.navigate('Quiz', { 
          title: 'Space', 
          questions: spaceQuestion,
          color: '#36b1f0',
        })
      } 
    />
    <RowItem 
      color='#799496' 
      name='Westerns' 
      onPress={() => 
        navigation.navigate('Quiz', { 
          title: 'Westerns', 
          questions: westernsQuestion,
          color: '#799496',
        })
      } 
    />
    <RowItem 
      color='#49475B' 
      name='Computers' 
      onPress={() => 
        navigation.navigate('Quiz', { 
          title: 'Computers', 
          questions: computerQuestion,
          color: '#49475B',
        })
      } 
    />
  </ScrollView>
)